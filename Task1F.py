from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

stations = build_station_list()

""" 
Use this code to test that lists are accurate

for i in stations:
    if i.name == 'Wilfholme PS':
        print(i) 
        
"""

print(inconsistent_typical_range_stations(stations = stations))
