from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

p = (52.2053, 0.1218)

stations = build_station_list()
print(stations_by_distance(stations=stations, p=p)[:10])
print(stations_by_distance(stations=stations, p=p)[-10:])
