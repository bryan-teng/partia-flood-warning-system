import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from .datafetcher import fetch_measure_levels
from .analysis import polyfit
import numpy as np
import matplotlib

def plot_water_levels(station, dates, levels):
    
    # Plot
    plt.plot(dates, levels, color = 'b')
    plt.axhline(y=station.typical_range[0], color = 'r')
    plt.axhline(y=station.typical_range[1], color = 'r')
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    #create empty tuples for dates and water level
    t = []
    level = []
    t_cleaned = []
    for i in dates:
        t_cleaned.append(matplotlib.dates.date2num(i))
    for i, j in zip(t_cleaned, levels):
        t.append(i - t_cleaned[0])
        level.append(j)
    #plot graph with 30 points within the interval t [0] and t[-1] 
    t1 = np.linspace(t[0], t[-1], 30)

    poly = polyfit(dates, levels, p)
    #plot the polynomial against time on a graph 
    plt.plot(t1, poly(t1), color = 'm')
    # return plot with station as name, water levels against date on the y and x-axes respectively
    plot_water_levels(station = station, dates = t, levels = level)