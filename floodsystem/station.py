# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}\n".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        try:
            if self.typical_range[1] < self.typical_range[0]:
                return True #if higher range > lower range = false 
            else:
                return False #otherwise true
        except:
            return True

    # Task 2B Code 
    def relative_water_level(self):
        # if data is inconsistent, return none
        if self.typical_range_consistent == True or self.latest_level == None:
            return None
        # if data is consistent, return the relative water level using the equation (latest water level - typical lower limit / typical upper limit - typical lower limit)
        else:
            relative_water = (self.latest_level - self.typical_range[0])/(self.typical_range[1]-self.typical_range[0])
            return relative_water
def inconsistent_typical_range_stations(stations):
    inconsistent_list = []
    for station in stations:
        # if there are inconsistent range values, append them into one list
        if station.typical_range_consistent() == True:
            inconsistent_list.append(station.name)
    inconsistent_list = sorted(inconsistent_list)
    return inconsistent_list
