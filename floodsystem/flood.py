def stations_level_over_threshold(stations, tol):
    # create new tuple to hold stations that has relative water level above tolerance
    flood_risk = []
    # within stations, if the data is of appropriate format, and has higher than relative water level higher than tolerance, add them into the tuple
    for station in stations:
        if station.typical_range_consistent() == False and station.relative_water_level() != None:
            if station.relative_water_level() > tol:
                flood_risk.append((station.name, station.relative_water_level()))
    # sort the tuple in descending order
    return sorted(flood_risk, key = lambda x: x[1], reverse = True) 

def stations_highest_rel_level(stations, N):
    # new tuple for stations at flood risk
    flood_risk = []
    for station in stations:
        # if data provided is consistent, append above tuple with station name and relative water level
        if station.typical_range_consistent() == False and station.relative_water_level() != None:
            flood_risk.append((station.name, station.relative_water_level()))
        # sort the tuple by descending relative water levels
    return sorted(flood_risk, key = lambda x: x[1], reverse = True)[:N] 