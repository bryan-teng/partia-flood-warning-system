import numpy as np
import matplotlib  
import matplotlib.pyplot as plt
from .station import MonitoringStation

def polyfit(dates, levels, p):
    #create empty tuples for date, water level and days passed
    t = []
    level = []
    t_cleaned = []
    # add days passed into t_cleaned tuple
    for i in dates:
        t_cleaned.append(matplotlib.dates.date2num(i))

    for i, j in zip(t_cleaned, levels):
        # add dates into tuple t and add water levels into tuple level
        t.append(i - t_cleaned[0])
        level.append(j)
    # find the most suitable coefficient for polynomial of degree p that is a best fit for data in level)
    p_coeff = np.polyfit(t, level, p)
    #construct polynomial with the above parameters 
    poly = np.poly1d(p_coeff)

    return poly

#Part 2G Code 
def town_data_cleaner(stations):
    #create new tuple and add station data as a string
    new_stations = []
    for i in stations:
        if type(i.town) == list:
            for j in i.town:
                new_stations.append(MonitoringStation(station_id = i.station_id, measure_id = i.measure_id, label = None, coord = i.coord, typical_range = i.typical_range, river = i.river, town = j))
        if i.town == None:
            continue
        if type(i.town) == str:
            new_stations.append(i)

    return new_stations

def stations_by_town(stations):
    town_dict = {}
    #Creating a dictionary of towns, adding towns from station data
    for station in stations:
        if station.town not in town_dict:
            town_dict[station.town] = [station]
        else:
            town_dict[station.town].append(station)

    return town_dict

def town_average_water(stations):
    counter = 0.0
    total_sum = 0.0
   
    for station in stations:
         # counting the number of stations within a town that has relative water levels that are high,
         # summing up number of stations and summing up total relative water levels
        if station.typical_range_consistent() == False and station.relative_water_level() != None:
            counter += 1
            total_sum += station.relative_water_level()
        # if there are one or more stations in a town with water level over tol., find the average 
        # relative water levels across all stations with water level over tol. in the town.
    if counter >= 1:
        avg_water = total_sum / counter
        return (station.name, avg_water)
        #otherwise, treat the station as not at risk at all 
    else:
        return (station.name, 0)

def send_help_now(stations):
    
    all_towns = set([stations[i].town for i in range(len(stations))])
    new_towns = list(all_towns)
    print(len(stations))

    town_and_stations = stations_by_town(stations)
    updated_town_list = []
    
    for town in new_towns:
        updated_town_list.append(town_average_water(town_and_stations[town]))

    sorted_town_list = sorted(updated_town_list, key = lambda x:x[1], reverse = True)

    for i in range(30):
        if sorted_town_list[i][1] >= 5:
            risk = 'severe'
        elif sorted_town_list[i][1] >= 2:
            risk = 'high'
        elif sorted_town_list[i][1] >= 1:
            risk = 'moderate'
        else:
            risk = 'low'
        print('The town {} has the number {} chance of a flood, with an average relative water level of {}.\n The flood risk level is {}.'.format(sorted_town_list[i][0], i+1, sorted_town_list[i][1], risk))
    






