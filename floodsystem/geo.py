# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine

def stations_by_distance(stations, p):
    #creates a new list of stations
    new_station_list = []

    #add only desired class attributes: station name, town and distance from p
    #Note that haversine returns distance in km
    for station in stations:
        distance_from_p = haversine(p, station.coord)
        new_station_list.append((station.name, station.town, distance_from_p))
    sorted_station_list = sorted(new_station_list, key = lambda x:x[2])
    return sorted_station_list

#Task 1C Code 
def stations_within_radius(stations, centre, r):
    #create a list of stations within the r of any given coordinates
    stations_near_me = []

    for station in stations: 
        distance_from_p = haversine(centre,station.coord) #evaluate distance from given coordinate
        if distance_from_p <= r: 
            stations_near_me.append(station.name) #create list of monitoring stations within r
        
    sorted_stations_near_me = sorted(stations_near_me) #sort list by distance
    return sorted_stations_near_me #return list sorted by distance

#Task 1D Code
def rivers_with_station(stations):
    #Sort river names into a unique set
    stations_rivers = set([stations[i].river for i in range(len(stations))])
    #Convert back into a sorted list
    stations_rivers = sorted(list(stations_rivers))
    return stations_rivers


def stations_by_river(stations):
    river_dict = {}
    #Creating a dictionary
    for station in stations:
        if station.river not in river_dict:
            river_dict[station.river] = [station.name]
        else:
            river_dict[station.river].append(station.name)
    #Sorting the dictionary
    for i in river_dict:
        river_dict[i].sort()
    return river_dict

#Task 1E Code

def rivers_by_station_number(stations, N):
    river_dict = {}
    #Creating a dictionary with key River Name and value number of monitoring stations
    for station in stations:
        if station.river not in river_dict:
            river_dict[station.river] = 1
        else:
            river_dict[station.river] += 1
    #Converting to a sorted list
    river_counted = []
    for i in river_dict:
        river_counted.append((i, river_dict[i]))
    river_sorted = sorted(river_counted, key = lambda x:x[1], reverse = True)

    #Adding the top N rivers to a new list
    new_river = []
    for i in range(N):
        new_river.append(river_sorted[i])

    #Accounting for tiebreaks
    counter = N-1
    while river_sorted[counter][1] == river_sorted[counter+1][1]:
        new_river.append(river_sorted[counter+1])
        counter += 1
        
    return new_river
