from floodsystem.analysis import send_help_now, stations_by_town, town_data_cleaner
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

stations = build_station_list()
updated_stations = update_water_levels(stations)

cleaned_stations = town_data_cleaner(updated_stations)

send_help_now(cleaned_stations)
