from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

stations = build_station_list()
#First Part
print(rivers_with_station(stations = stations)[:10])
#Second Part
print((stations_by_river(stations = stations)['River Aire']))
print((stations_by_river(stations = stations)['River Cam']))
print((stations_by_river(stations = stations)['River Thames']))