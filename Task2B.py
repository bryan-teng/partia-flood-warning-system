from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

stations = build_station_list()
updated_stations = update_water_levels(stations)

flood_risk_list = stations_level_over_threshold(updated_stations, 0.8)
for i in flood_risk_list:
    print(str(i[0]) + ' ' + str(i[1]))

