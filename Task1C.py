from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

p = (52.2053, 0.1218)
r = 10

stations = build_station_list()

station_radius_list = stations_within_radius(stations = stations, centre = p, r = r)
print(station_radius_list[:10])
