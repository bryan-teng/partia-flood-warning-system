"""Unit tests for functions within the geo.py module"""
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.geo import rivers_by_station_number
from haversine import haversine
import numpy as np


def test_task1b():
    stations = build_station_list()

    p = (52.2053, 0.1218)
    task1b = stations_by_distance(stations = stations, p = p)

    #Test that the function returns a list of tuples, for the following index:
    # 0 = Name, 2 = Distance in km. Town omitted because insignificant
    for i in task1b:
        assert type(i[0]) == str
        assert type(i[2]) == float

    #Test that the distances are in ascending order

    for j in range(len(task1b)-1):
        assert task1b[j][2] <= task1b[j+1][2]

def test_task1c():
    stations = build_station_list()
    p = (52.2053, 0.1218)
    r = 10

    counter = 0

    task1c = stations_within_radius(stations = stations, centre = p, r = r)
    for station in stations:
        distance_from_p = haversine(p,station.coord)
        if distance_from_p <= r:
            counter += 1
    #Check that the distances are less than r
    assert counter == len(task1c)

def test_task1d():
    stations = build_station_list()
    
    task1d1 = rivers_with_station(stations)
    task1d2 = stations_by_river(stations)

    #Checking that all rivers from function rivers_with_station has a corresponding monitoring station
    #from function stations_by_river
    for i in task1d1:
        assert i in task1d2

def test_task1e():
    
    stations = build_station_list()
    #N = np.random.randint(2,20)
    N = 11
    
    task1e =  rivers_by_station_number(stations, N)

    #Test that the list is in descending order
    for j in range(len(task1e)-1):
        if len(task1e) == N:
            assert task1e[j][1] >= task1e[j+1][1] #Checking that the ones higher in the list are larger than the ones lower in the list
        #Test Tiebreaker situation
        if len(task1e) > N:
            for i in range(len(task1e) - N - 1):
                assert task1e[-(len(task1e) -N) + i][1] == task1e[-(len(task1e) -N)+ i + 1][1] # For case where one river has same monitoring stations as the next