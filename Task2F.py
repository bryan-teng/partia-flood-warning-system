#import relevant data

from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from datetime import datetime, timedelta
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
import matplotlib.pyplot as plt

#Create new list with the latest water levels
stations = build_station_list()
updated_stations = update_water_levels(stations)
#Add the 5 most at risk stations into the list
flood_risk_list = stations_highest_rel_level(updated_stations, N = 5)

#create new tuple and add the entries in the previes list into it
plot_list = []
for i in flood_risk_list:
    plot_list.append(i[0])

#for station that is 5 most at risk, plot water level against time 
for station in stations:
    if station.name in plot_list:
        #retrieve data over past 10 days
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days = dt))
        #create new list to contain the latest dates matched to their water levels at that day
        t = []
        level = []
        #from the obtained data, sort time and water level for the given station
        for i,j in zip(dates, levels):
            t.append(i)
            level.append(j)
        #plot the graph of water level against date with station.name as the title
        plot_water_level_with_fit(station= station, dates = t, levels = level, p = 4) 